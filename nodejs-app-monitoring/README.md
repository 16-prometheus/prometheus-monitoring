#### This project is for the Devops bootcamp module "Monitoring"

##### Metrics
The project exposes metrics on /metrics endpoint

To run the nodejs application:

    npm install 
    node app/server.js

To build the project:

    docker build -t repo-name/image-name:image-tag .
    docker push repo-name/image-name:image-tag



### Demo Project:
Configure Monitoring for Own Application
### Technologies used:
Prometheus, Kubernetes, Node.js, Grafana, Docker, Docker Hub
### Project Description:
- Configure our NodeJS application to collect & expose Metrics with Prometheus Client Library
    - This is configured in app/server.js file
    - https://github.com/siimon/prom-client node client was used
- Deploy the NodeJS application, which has a metrics endpoint configured, into Kubernetes cluster 
    - Configure docker registry in k8s cluster 
        - kubectl create secret docker-registry my-registry-key --docker-server=https://index.docker.io/v2/ --docker-username cholkuany docker-password xxxx
        - Adjust __imagePullSecrets__ in k8s-config file by adding the registry key name e.g. my-registry-key as shown above.
    - Use k8s-config.yaml - adjust image repo accordingly.
- Configure Prometheus to scrape this exposed metrics and visualize it in Grafana Dashboard
    - configure service monitor - see k8s-config.yaml for configuration
    - In Grafana dashboard configure metric graphical panels for the metrics which are configured in app/server.js file:
        - http_request_duration_seconds - rate(http_request_duration_seconds[2])
        - http_request_operations_total - rate(http_request_operations_total[2])