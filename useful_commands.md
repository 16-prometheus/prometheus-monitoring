### Deploy MS in EKS
    eksctl create cluster - EKS cluster "floral-rainbow-1699374558" in "ca-central-1" region is ready
    kubectl create namespace online-shop
    kubectl apply -f /Users/peterkuany/Desktop/DevOps/Kubernetes/online-shop-microservices-deployment/k8s-config/config-best-practices.yaml -n online-shop

# OPTIONAL for Linode
    chmod 400 ~/Downloads/online-shop-kubeconfig.yaml
    export KUBECONFIG=~/Downloads/online-shop-kubeconfig.yaml


### Deploy Prometheus Operator Stack
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo update
    kubectl create namespace monitoring
    helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring
    helm ls

[Link to the chart: https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack]

### Check Prometheus Stack Pods
    kubectl get all -n monitoring

### Access Prometheus UI
    kubectl port-forward svc/monitoring-kube-prometheus-prometheus 9090:9090 -n monitoring &

### Access Grafana
    kubectl port-forward svc/monitoring-grafana 8080:80 -n monitoring &
    user: admin
    pwd: prom-operator

### Trigger CPU spike with many requests
kubectl run curl-test-simulation --image=radial/busyboxplus:curl -i --tty --rm 

##### Deploy a busybox pod so we can curl our application 
    kubectl run curl-test --image=radial/busyboxplus:curl -i --tty --rm

##### create a script which curls the application endpoint. The endpoint is the external loadbalancer service endpoint
    for i in $(seq 1 10000)
    do
      curl a33cfb128085e4f5a840f1fb49d0b382-743473513.ca-central-1.elb.amazonaws.com:80 > test.txt
    done
### make test.sh executable
chmod +x test.sh

### Access Alert manager UI
    kubectl port-forward -n monitoring svc/monitoring-kube-prometheus-alertmanager 9093:9093 &

#### Create cpu stress
    kubectl delete pod cpu-test; 
    kubectl run cpu-test --image=containerstack/cpustress -- --cpu 4 --timeout 60s --metrics-brief


### Deploy Redis Exporter
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo add stable https://charts.helm.sh/stable
    helm repo update

    helm install redis-exporter prometheus-community/prometheus-redis-exporter -f redis-values.yaml

kubectl get PrometheusRule -n monitoring
kubectl apply -f alert-rules.yaml 

docker info
kubectl create secret docker-registry my-registry-key --docker-server=https://index.docker.io/v2/ --docker-username cholkuany docker-password xxxx